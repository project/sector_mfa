<?php

namespace Drupal\sector_mfa;

use Defuse\Crypto\Key as DefuseKey;
use  \Drupal\Component\Utility\Crypt;
use Drupal\Component\Utility\Random;
use Drupal\encrypt\Entity\EncryptionProfile;
use Drupal\key\Entity\Key;
use Drupal\user\Entity\Role;

class SectorMFAInstallHelpers {
  public static function createKey() {
    $random = new Random();
    $randomString = $random->string('32', TRUE);
    $values = [
      'id' => 'sector_mfa_key',
      'label' => 'Sector MFA key',
      'key_type' => "encryption",
      'key_type_settings' => ['key_size' => '256', 'base64_encoded' => TRUE],
      'key_provider' => 'config',
      'key_provider_settings' => ['key_value' => $randomString],
    ];
    Key::create($values)->save();
  }

  public static function createProfile() {
    $values = [
      'id' => 'sector_mfa_profile',
      'label' => 'Sector MFA profile',
      'encryption_method' => 'real_aes',
      'encryption_key' => 'sector_mfa_key',
    ];
    EncryptionProfile::create($values)->save();
  }

  public static function setTfaConfig() {
    $configFactory = \Drupal::configFactory();
    $config = $configFactory->getEditable('tfa.settings');
    $config->set('enabled', 1);
    $config->set('allowed_validation_plugins', ['ga_login_totp' => 'ga_login_totp']);
    $config->set('default_validation_plugin', 'ga_login_totp');
    $config->set('required_roles', ['authenticated' => 'authenticated']);
    $config->set('encryption', 'sector_mfa_profile');
    $config->save(TRUE);
  }

  public static function setPermissions() {
    $role = Role::load('authenticated');
    $role->grantPermission('setup own tfa');
    $role->save();
  }
}
